#include <stdio.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdbool.h>

/* Rules:
- If child fails to fork, parent should not hang (do I need to detach?)
- Failure to spawn mongocryptd need not error.
*/

void die (const char* format, ...) {
    FILE *fp;
    va_list args;

    fp = fopen ("deadlog.txt", "w");
    va_start (args, format);
    vfprintf (fp, format, args);
    va_end (args);
    exit (1);
}

/* Note, if mongocryptd fails to spawn (due to not being found on the path), an error is not reported. Users will get an error later, upon first attempt to use mongocryptd. */
/* These comments refer to three distinct processes: parent, child, and mongocryptd.
 * - parent is initial calling process
 * - child is the first forked child. It fork-execs mongocryptd then terminates. This makes mongocryptd an orphan, making it immediately adopted by the init process.
 * - mongocryptd is the final background daemon (grandchild process).
 * 
 * Returns false on failure and sets error
 */

/* TODO: replace die's with exits, and printfs with bson_set_error */
bool spawn_mongocryptd (char** args /* TODO error */) {
    pid_t pid;
    int fd;

    if (!args[0] || 0 != strcmp (args[0], "mongocryptd")) {
        /* TODO set error. */
        return false;
    }

    /* Fork. The child will terminate immediately (after fork-exec'ing mongocryptd). This orphans mongocryptd, and allows parent to wait on child. */
    pid = fork();
    if (pid < 0) {
        printf ("[parent] failed to fork (errno=%d)",errno);
        return false;
    } else if (pid > 0) {
        int child_status;

        /* Child will spawn mongocryptd and immediately terminate to turn mongocryptd into an orphan. */
        if (waitpid (pid, &child_status, 0 /* options */) < 0) {
            printf ("[parent] failed to wait for child (errno=%d)", errno);
        }
        return false;
    }

    /* Start a new session for the child, so it is not bound to the current session (e.g. terminal session). */
    if (setsid() < 0) {
        die("[child] failed to become a session leader (errno=%d)", errno);
        return false;
    }

    /* Fork again. Child terminates so mongocryptd gets orphaned and immedately adopted by init. */
    signal (SIGHUP, SIG_IGN);
    pid = fork();
    if (pid < 0) {
        die("[child] failed to fork while daemonising (errno=%d)",errno);
    } else if (pid > 0) {
        printf ("[child] spawning daemon with pid: %d. Exiting\n", pid);
        _exit(0);
    }

    /* Depending on the outcome of MONGOCRYPT-115, possibly change the process's working directory
     * with chdir like: `chdir (default_pid_path)`. Currently pid file ends up in application's
     * working directory.
     */

    /* Set the user file creation mask to zero. */
    umask(0);

    /* Close and reopen stdin. */
    fd = open("/dev/null", O_RDONLY);
    if (fd < 0) {
        die("[grandchild] failed to reopen stdin while daemonising (errno=%d)", errno);
    }
    dup2 (fd, STDIN_FILENO);
    close (fd);

    /* Close and reopen stdout. */
    fd = open("/dev/null", O_WRONLY);
    if (fd < 0) {
        die("[grandchild] failed to reopen stdout while daemonising (errno=%d)", errno);
    }
    dup2(fd, STDOUT_FILENO);
    close (fd);

    /* Close and reopen stderr. */
    fd = open("/dev/null", O_RDWR);
    if (fd < 0) {
        die("[grandchild] failed to reopen stderr while daemonising (errno=%d)", errno);
    }
    dup2(fd, STDERR_FILENO);
    close (fd);

    if (execvp ("mongocryptd", args) < 0) {
       /* Need to exit. */
       die ("[grandchild] failed: %d\n", errno);
    }
    
    /* Will never execute. */
    return false;
}

void old () {
    pid_t parent = getpid ();
    printf ("[parent] PID=%d\n", parent);

    pid_t child = fork ();
    char *args[3] = { "ls", NULL };

    /* parent should redirect stdout and stderr of child */
    if (child == 0) {
        // pid_t grandchild = fork();
        // if (grandchild == 0) {
            printf ("[child] running mongocryptd\n");
            /* pass arguments */
            int status = execv ("/bin/ls", args);
            
        // } else {
        //     exit(0);
        // }
    } else {
        printf ("[parent] child has PID: %d\n", child);
        /* wait (NULL); */
    }

    /* after exiting, mongocryptd process should be orphaned, have parent process of init */
    int i = 0;
    while (i < 30) {
        sleep(1);
        printf("parent waiting\n");
        i++;
        
    }
    printf ("[parent]\n");
}

int main (int argc, char** argv) {
    char *args[] = { "mongocryptd", "--idleShutdownTimeoutSecs=60", NULL };
    spawn_mongocryptd (args);
}