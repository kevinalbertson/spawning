#include <stdio.h>
#include <unistd.h>
#include <string.h>

/*
A FILE* wraps around a file descriptor and includes buffering.
I suspect you can shoot yourself in the foot if you conflate the two.
Examples
1. Open a FILE*, then close it's corresponding file descriptor. That leaks, because buffer struct is still allocated.
2. What if you do (1) then reopen the same file descriptor? Will that use same buffering? Let's find out. (Apparently not).
*/

int main (int argc, char** argv) {
    char stream_buf[1024] = {0};

    int saved_stdout = dup(fileno(stdout));

    printf ("stdout at %d\n", fileno(stdout));
    printf ("stdout saved at %d\n", saved_stdout);

    /* Redirect stdout to /dev/null */
    stdout = freopen ("/dev/null", "a", stdout);
    setbuf (stdout, stream_buf);

    printf ("test\n");
    
    /* restore stdout */
    fclose (stdout);
    stdout = fdopen (saved_stdout, "w");

    printf ("captured: %s\n", stream_buf);
}